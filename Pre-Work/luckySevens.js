function myFunction() {
	var startingBet;
	var highestTotal;
	var currentTotal;
	startingBet = parseFloat(document.getElementById("startingBet").value);
	currentTotal = startingBet;
	highestTotal = startingBet;
	var rollNumber;
	rollNumber=0;
	var rollPeak;
	rollPeak = 0;
	var error;
	
	if (isNaN(startingBet)) {
		error = "That is not a valid number"
		document.getElementById("error").innerHTML = error;
	} else if (startingBet <= 0 || startingBet >100) {
		error = "Please enter a number between 1 and 100";
		document.getElementById("error").innerHTML = error;
	} else {
	while (currentTotal > 0) {
		rollNumber ++;
		var roll1, roll2;
		roll1 = Math.floor(Math.random() * 6) + 1;
		roll2 = Math.floor(Math.random() * 6) + 1;
		var totalRoll;
		totalRoll = roll1 + roll2;
			if(totalRoll == 7) {
				currentTotal = currentTotal + 4;
			}
			else {
			currentTotal --;
			}

			if (currentTotal>=highestTotal) {
				highestTotal=currentTotal
				rollPeak=rollNumber
			}

		}
		document.getElementById("results").style.display = "";
		document.getElementById("Bet1").innerHTML = "$" + startingBet;
		document.getElementById("rolls").innerHTML = rollNumber;
		document.getElementById("highest").innerHTML = "$" + highestTotal;
		document.getElementById("peakroll").innerHTML = rollPeak;
		document.getElementById("play").innerHTML = "Play Again";
	}
}